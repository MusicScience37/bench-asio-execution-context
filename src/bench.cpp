#include <memory>

#include <stat_bench/benchmark_macros.h>

#include "client/echo_client.h"
#include "server/multi_context_echo_server.h"
#include "server/multi_thread_echo_server.h"
#include "server/single_thread_echo_server.h"
#include "server/thread_pool_echo_server.h"

class Fixture : public stat_bench::FixtureBase {
public:
    Fixture() { add_threads_param()->add(1)->add(2)->add(4); }

    void bench(stat_bench::bench::InvocationContext& STAT_BENCH_CONTEXT_NAME,
        const asio::ip::tcp::endpoint& server_endpoint) {
        std::vector<std::unique_ptr<client::EchoClient>> clients;
        for (std::size_t i = 0; i < STAT_BENCH_CONTEXT_NAME.threads(); ++i) {
            clients.push_back(std::make_unique<client::EchoClient>(
                server_endpoint, data_.size()));
        }

        STAT_BENCH_MEASURE_INDEXED(
            thread_ind, /*sample_ind*/, /*iteration_ind*/) {
            clients.at(thread_ind)->call(data_);
        };
    }

    //! Data size.
    static constexpr std::size_t data_size = 128;

    //! Data.
    std::vector<char> data_ = std::vector<char>(data_size, 'a');

    //! Server endpoint.
    asio::ip::tcp::endpoint server_endpoint_{
        asio::ip::address_v4::loopback(), 0};
};

STAT_BENCH_CASE_F(Fixture, "echo", "single thread") {
    server::SingleThreadEchoServer server{server_endpoint_, data_.size()};
    server.start();
    bench(STAT_BENCH_CONTEXT_NAME, server.server_endpoint());
    server.stop();
}

STAT_BENCH_CASE_F(Fixture, "echo", "multi thread, single context") {
    constexpr std::size_t num_threads = 4;
    server::MultiThreadEchoServer server{
        server_endpoint_, data_.size(), num_threads};
    server.start();
    bench(STAT_BENCH_CONTEXT_NAME, server.server_endpoint());
    server.stop();
}

STAT_BENCH_CASE_F(Fixture, "echo", "asio::thread_pool") {
    constexpr std::size_t num_threads = 4;
    server::ThreadPoolEchoServer server{
        server_endpoint_, data_.size(), num_threads};
    server.start();
    bench(STAT_BENCH_CONTEXT_NAME, server.server_endpoint());
    server.stop();
}

STAT_BENCH_CASE_F(Fixture, "echo", "multiple context") {
    constexpr std::size_t num_threads = 4;
    server::MultiContextEchoServer server{
        server_endpoint_, data_.size(), num_threads};
    server.start();
    bench(STAT_BENCH_CONTEXT_NAME, server.server_endpoint());
    server.stop();
}

STAT_BENCH_MAIN
