#pragma once

#include <asio/io_context.hpp>
#include <asio/ip/tcp.hpp>

namespace client {

/*!
 * \brief Class of echo client.
 */
class EchoClient {
public:
    //! Protocol.
    using protocol = asio::ip::tcp;

    /*!
     * \brief Constructor.
     *
     * \param[in] server_endpoint Server endpoint.
     * \param[in] data_size Data size.
     */
    EchoClient(protocol::endpoint server_endpoint, std::size_t data_size);

    /*!
     * \brief Call.
     *
     * \param[in] request Request.
     */
    void call(const std::vector<char>& request);

    /*!
     * \brief Get the response data.
     *
     * \return Response data.
     */
    [[nodiscard]] auto response() const noexcept -> const std::vector<char>&;

    /*!
     * \brief Close the connection.
     */
    void close();

private:
    //! Execution context.
    asio::io_context context_;

    //! Socket.
    protocol::socket socket_;

    //! Response data.
    std::vector<char> response_;
};

}  // namespace client
