#include "client/echo_client.h"

#include <asio/buffer.hpp>
#include <asio/read.hpp>
#include <asio/write.hpp>

namespace client {

EchoClient::EchoClient(
    protocol::endpoint server_endpoint, std::size_t data_size)
    : context_(), socket_(context_), response_(data_size) {
    socket_.connect(server_endpoint);
}

void EchoClient::call(const std::vector<char>& request) {
    asio::write(socket_, asio::const_buffer(request.data(), request.size()));
    asio::read(
        socket_, asio::mutable_buffer(response_.data(), response_.size()));
}

auto EchoClient::response() const noexcept -> const std::vector<char>& {
    return response_;
}

void EchoClient::close() { socket_.close(); }

}  // namespace client
