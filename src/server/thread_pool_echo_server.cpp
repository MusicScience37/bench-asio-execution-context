#include "server/thread_pool_echo_server.h"

#include <chrono>
#include <exception>
#include <functional>
#include <iostream>
#include <thread>

#include <asio/bind_executor.hpp>
#include <asio/post.hpp>
#include <asio/read.hpp>
#include <asio/thread_pool.hpp>
#include <asio/write.hpp>

namespace server {

ThreadPoolEchoServerConnection::ThreadPoolEchoServerConnection(
    asio::thread_pool& context, std::size_t data_size)
    : socket_(context),
      strand_(asio::make_strand(context)),
      request_(data_size) {}

auto ThreadPoolEchoServerConnection::create(asio::thread_pool& context,
    std::size_t data_size) -> std::shared_ptr<ThreadPoolEchoServerConnection> {
    return std::make_shared<ThreadPoolEchoServerConnection>(context, data_size);
}

auto ThreadPoolEchoServerConnection::socket() noexcept -> protocol::socket& {
    return socket_;
}

void ThreadPoolEchoServerConnection::start() { do_read(); }

void ThreadPoolEchoServerConnection::do_read() {
    using std::placeholders::_1;
    using std::placeholders::_2;
    asio::async_read(socket_,
        asio::mutable_buffer(request_.data(), request_.size()),
        asio::bind_executor(strand_,
            std::bind(&ThreadPoolEchoServerConnection::on_read,
                shared_from_this(), _1, _2)));
}

void ThreadPoolEchoServerConnection::on_read(
    const asio::error_code& error, std::size_t /*bytes_transferred*/) {
    if (error) {
        return;
    }

    using std::placeholders::_1;
    using std::placeholders::_2;
    asio::async_write(socket_,
        asio::const_buffer(request_.data(), request_.size()),
        asio::bind_executor(strand_,
            std::bind(&ThreadPoolEchoServerConnection::on_write,
                shared_from_this(), _1, _2)));
}

void ThreadPoolEchoServerConnection::on_write(
    const asio::error_code& error, std::size_t /*bytes_transferred*/) {
    if (error) {
        return;
    }

    do_read();
}

ThreadPoolEchoServer::ThreadPoolEchoServer(
    const protocol::endpoint& server_endoint, std::size_t data_size,
    std::size_t num_threads)
    : context_(num_threads),
      strand_(asio::make_strand(context_)),
      acceptor_(context_, server_endoint),
      connection_(),
      data_size_(data_size),
      is_running_(false) {}

ThreadPoolEchoServer::~ThreadPoolEchoServer() { stop(); }

auto ThreadPoolEchoServer::server_endpoint() -> protocol::endpoint {
    return acceptor_.local_endpoint();
}

void ThreadPoolEchoServer::start() {
    if (is_running_.exchange(true)) {
        return;
    }

    do_accept();
}

void ThreadPoolEchoServer::stop() {
    if (!is_running_.exchange(false)) {
        return;
    }

    asio::post(
        context_, asio::bind_executor(strand_, [this] { acceptor_.cancel(); }));
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    context_.stop();
    context_.join();
}

void ThreadPoolEchoServer::do_accept() {
    connection_ = ThreadPoolEchoServerConnection::create(context_, data_size_);
    using std::placeholders::_1;
    acceptor_.async_accept(connection_->socket(),
        asio::bind_executor(
            strand_, std::bind(&ThreadPoolEchoServer::on_accept, this, _1)));
}

void ThreadPoolEchoServer::on_accept(const asio::error_code& /*error*/) {
    connection_->start();
    do_accept();
}

}  // namespace server
