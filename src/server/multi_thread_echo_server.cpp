#include "server/multi_thread_echo_server.h"

#include <chrono>
#include <exception>
#include <functional>
#include <iostream>
#include <thread>

#include <asio/bind_executor.hpp>
#include <asio/io_context.hpp>
#include <asio/post.hpp>
#include <asio/read.hpp>
#include <asio/write.hpp>

namespace server {

MultiThreadEchoServerConnection::MultiThreadEchoServerConnection(
    asio::io_context& context, std::size_t data_size)
    : socket_(context),
      strand_(asio::make_strand(context)),
      request_(data_size) {}

auto MultiThreadEchoServerConnection::create(asio::io_context& context,
    std::size_t data_size) -> std::shared_ptr<MultiThreadEchoServerConnection> {
    return std::make_shared<MultiThreadEchoServerConnection>(
        context, data_size);
}

auto MultiThreadEchoServerConnection::socket() noexcept -> protocol::socket& {
    return socket_;
}

void MultiThreadEchoServerConnection::start() { do_read(); }

void MultiThreadEchoServerConnection::do_read() {
    using std::placeholders::_1;
    using std::placeholders::_2;
    asio::async_read(socket_,
        asio::mutable_buffer(request_.data(), request_.size()),
        asio::bind_executor(strand_,
            std::bind(&MultiThreadEchoServerConnection::on_read,
                shared_from_this(), _1, _2)));
}

void MultiThreadEchoServerConnection::on_read(
    const asio::error_code& error, std::size_t /*bytes_transferred*/) {
    if (error) {
        return;
    }

    using std::placeholders::_1;
    using std::placeholders::_2;
    asio::async_write(socket_,
        asio::const_buffer(request_.data(), request_.size()),
        asio::bind_executor(strand_,
            std::bind(&MultiThreadEchoServerConnection::on_write,
                shared_from_this(), _1, _2)));
}

void MultiThreadEchoServerConnection::on_write(
    const asio::error_code& error, std::size_t /*bytes_transferred*/) {
    if (error) {
        return;
    }

    do_read();
}

MultiThreadEchoServer::MultiThreadEchoServer(
    const protocol::endpoint& server_endoint, std::size_t data_size,
    std::size_t num_threads)
    : context_(),
      strand_(asio::make_strand(context_)),
      num_threads_(num_threads),
      acceptor_(context_, server_endoint),
      connection_(),
      data_size_(data_size),
      is_running_(false) {}

MultiThreadEchoServer::~MultiThreadEchoServer() { stop(); }

auto MultiThreadEchoServer::server_endpoint() -> protocol::endpoint {
    return acceptor_.local_endpoint();
}

void MultiThreadEchoServer::start() {
    if (is_running_.exchange(true)) {
        return;
    }

    do_accept();

    for (std::size_t i = 0; i < num_threads_; ++i) {
        threads_.emplace_back([this] {
            while (true) {
                try {
                    context_.run();
                    return;
                } catch (const std::exception& e) {
                    std::cerr << "Exception: " << e.what() << std::endl;
                }
            }
        });
    }
}

void MultiThreadEchoServer::stop() {
    if (!is_running_.exchange(false)) {
        return;
    }

    asio::post(
        context_, asio::bind_executor(strand_, [this] { acceptor_.cancel(); }));
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    context_.stop();
    for (auto& thread : threads_) {
        thread.join();
    }
    threads_.clear();
}

void MultiThreadEchoServer::do_accept() {
    connection_ = MultiThreadEchoServerConnection::create(context_, data_size_);
    using std::placeholders::_1;
    acceptor_.async_accept(connection_->socket(),
        asio::bind_executor(
            strand_, std::bind(&MultiThreadEchoServer::on_accept, this, _1)));
}

void MultiThreadEchoServer::on_accept(const asio::error_code& /*error*/) {
    connection_->start();
    do_accept();
}

}  // namespace server
