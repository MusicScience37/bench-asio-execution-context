#include "server/multi_context_echo_server.h"

#include <chrono>
#include <exception>
#include <functional>
#include <iostream>
#include <thread>

#include <asio/executor_work_guard.hpp>
#include <asio/io_context.hpp>
#include <asio/post.hpp>
#include <asio/read.hpp>
#include <asio/write.hpp>

namespace server {

MultiContextEchoServerConnection::MultiContextEchoServerConnection(
    asio::io_context& context, std::size_t data_size)
    : socket_(context), request_(data_size) {}

auto MultiContextEchoServerConnection::create(
    asio::io_context& context, std::size_t data_size)
    -> std::shared_ptr<MultiContextEchoServerConnection> {
    return std::make_shared<MultiContextEchoServerConnection>(
        context, data_size);
}

auto MultiContextEchoServerConnection::socket() noexcept -> protocol::socket& {
    return socket_;
}

void MultiContextEchoServerConnection::start() { do_read(); }

void MultiContextEchoServerConnection::do_read() {
    using std::placeholders::_1;
    using std::placeholders::_2;
    asio::async_read(socket_,
        asio::mutable_buffer(request_.data(), request_.size()),
        std::bind(&MultiContextEchoServerConnection::on_read,
            shared_from_this(), _1, _2));
}

void MultiContextEchoServerConnection::on_read(
    const asio::error_code& error, std::size_t /*bytes_transferred*/) {
    if (error) {
        return;
    }

    using std::placeholders::_1;
    using std::placeholders::_2;
    asio::async_write(socket_,
        asio::const_buffer(request_.data(), request_.size()),
        std::bind(&MultiContextEchoServerConnection::on_write,
            shared_from_this(), _1, _2));
}

void MultiContextEchoServerConnection::on_write(
    const asio::error_code& error, std::size_t /*bytes_transferred*/) {
    if (error) {
        return;
    }

    do_read();
}

MultiContextEchoServer::ConnectionContext::ConnectionContext()
    : context(), work_guard(asio::make_work_guard(context)), thread() {
    thread = std::thread([this] { context.run(); });
}

static std::vector<std::unique_ptr<MultiContextEchoServer::ConnectionContext>>
create_connection_context(std::size_t num_threads) {
    std::vector<std::unique_ptr<MultiContextEchoServer::ConnectionContext>>
        list;
    for (std::size_t i = 0; i < num_threads; ++i) {
        list.push_back(
            std::make_unique<MultiContextEchoServer::ConnectionContext>());
    }
    return list;
}

MultiContextEchoServer::MultiContextEchoServer(
    const protocol::endpoint& server_endoint, std::size_t data_size,
    std::size_t num_threads)
    : connection_context_(create_connection_context(num_threads)),
      next_connection_context_ind_(1),
      acceptor_(connection_context_.at(0)->context, server_endoint),
      connection_(),
      data_size_(data_size),
      is_running_(false) {}

MultiContextEchoServer::~MultiContextEchoServer() { stop(); }

auto MultiContextEchoServer::server_endpoint() -> protocol::endpoint {
    return acceptor_.local_endpoint();
}

void MultiContextEchoServer::start() {
    if (is_running_.exchange(true)) {
        return;
    }

    do_accept();
}

void MultiContextEchoServer::stop() {
    if (!is_running_.exchange(false)) {
        return;
    }

    asio::post(
        connection_context_.at(0)->context, [this] { acceptor_.cancel(); });
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    for (auto& obj : connection_context_) {
        obj->context.stop();
        obj->thread.join();
    }
}

void MultiContextEchoServer::do_accept() {
    connection_ = MultiContextEchoServerConnection::create(
        connection_context_.at(next_connection_context_ind_)->context,
        data_size_);

    ++next_connection_context_ind_;
    if (next_connection_context_ind_ >= connection_context_.size()) {
        next_connection_context_ind_ = 0;
    }

    using std::placeholders::_1;
    acceptor_.async_accept(connection_->socket(),
        std::bind(&MultiContextEchoServer::on_accept, this, _1));
}

void MultiContextEchoServer::on_accept(const asio::error_code& /*error*/) {
    connection_->start();
    do_accept();
}

}  // namespace server
