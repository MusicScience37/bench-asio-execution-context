#include "server/thread_pool_echo_server.h"

#include <catch2/catch_test_macros.hpp>

#include "client/echo_client.h"

TEST_CASE("ThreadPoolEchoServerConnection") {
    using protocol = server::ThreadPoolEchoServer::protocol;

    SECTION("call") {
        const auto data = std::vector<char>{'a', 'b', 'c', 'd', 'e', 'f'};
        const std::size_t data_size = data.size();

        constexpr std::size_t num_threads = 4;
        server::ThreadPoolEchoServer server{
            protocol::endpoint(asio::ip::address_v4::loopback(), 0), data_size,
            num_threads};
        server.start();

        client::EchoClient client{server.server_endpoint(), data_size};
        REQUIRE_NOTHROW(client.call(data));
        CHECK(client.response() == data);

        client.close();
        server.stop();
    }
}
