#include "server/single_thread_echo_server.h"

#include <chrono>
#include <exception>
#include <functional>
#include <iostream>
#include <thread>

#include <asio/io_context.hpp>
#include <asio/post.hpp>
#include <asio/read.hpp>
#include <asio/write.hpp>

namespace server {

SingleThreadEchoServerConnection::SingleThreadEchoServerConnection(
    asio::io_context& context, std::size_t data_size)
    : socket_(context), request_(data_size) {}

auto SingleThreadEchoServerConnection::create(
    asio::io_context& context, std::size_t data_size)
    -> std::shared_ptr<SingleThreadEchoServerConnection> {
    return std::make_shared<SingleThreadEchoServerConnection>(
        context, data_size);
}

auto SingleThreadEchoServerConnection::socket() noexcept -> protocol::socket& {
    return socket_;
}

void SingleThreadEchoServerConnection::start() { do_read(); }

void SingleThreadEchoServerConnection::do_read() {
    using std::placeholders::_1;
    using std::placeholders::_2;
    asio::async_read(socket_,
        asio::mutable_buffer(request_.data(), request_.size()),
        std::bind(&SingleThreadEchoServerConnection::on_read,
            shared_from_this(), _1, _2));
}

void SingleThreadEchoServerConnection::on_read(
    const asio::error_code& error, std::size_t /*bytes_transferred*/) {
    if (error) {
        return;
    }

    using std::placeholders::_1;
    using std::placeholders::_2;
    asio::async_write(socket_,
        asio::const_buffer(request_.data(), request_.size()),
        std::bind(&SingleThreadEchoServerConnection::on_write,
            shared_from_this(), _1, _2));
}

void SingleThreadEchoServerConnection::on_write(
    const asio::error_code& error, std::size_t /*bytes_transferred*/) {
    if (error) {
        return;
    }

    do_read();
}

SingleThreadEchoServer::SingleThreadEchoServer(
    const protocol::endpoint& server_endoint, std::size_t data_size)
    : context_(),
      thread_(),
      acceptor_(context_, server_endoint),
      connection_(),
      data_size_(data_size),
      is_running_(false) {}

SingleThreadEchoServer::~SingleThreadEchoServer() { stop(); }

auto SingleThreadEchoServer::server_endpoint() -> protocol::endpoint {
    return acceptor_.local_endpoint();
}

void SingleThreadEchoServer::start() {
    if (is_running_.exchange(true)) {
        return;
    }

    do_accept();

    thread_ = std::thread([this] {
        while (true) {
            try {
                context_.run();
                return;
            } catch (const std::exception& e) {
                std::cerr << "Exception: " << e.what() << std::endl;
            }
        }
    });
}

void SingleThreadEchoServer::stop() {
    if (!is_running_.exchange(false)) {
        return;
    }

    asio::post(context_, [this] { acceptor_.cancel(); });
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    context_.stop();
    thread_.join();
}

void SingleThreadEchoServer::do_accept() {
    connection_ =
        SingleThreadEchoServerConnection::create(context_, data_size_);
    using std::placeholders::_1;
    acceptor_.async_accept(connection_->socket(),
        std::bind(&SingleThreadEchoServer::on_accept, this, _1));
}

void SingleThreadEchoServer::on_accept(const asio::error_code& /*error*/) {
    connection_->start();
    do_accept();
}

}  // namespace server
