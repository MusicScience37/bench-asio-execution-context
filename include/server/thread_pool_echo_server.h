#pragma once

#include <atomic>
#include <memory>
#include <thread>
#include <vector>

#include <asio/error_code.hpp>
#include <asio/ip/tcp.hpp>
#include <asio/strand.hpp>
#include <asio/thread_pool.hpp>

namespace server {

/*!
 * \brief Class of connections in echo server.
 */
class ThreadPoolEchoServerConnection
    : public std::enable_shared_from_this<ThreadPoolEchoServerConnection> {
public:
    //! Protocol.
    using protocol = asio::ip::tcp;

    /*!
     * \brief Constructor.
     *
     * \param[in] context Execution context.
     * \param[in] data_size Data size.
     */
    ThreadPoolEchoServerConnection(
        asio::thread_pool& context, std::size_t data_size);

    /*!
     * \brief Create a connection.
     *
     * \param[in] context Execution context.
     * \param[in] data_size Data size.
     * \return Connection.
     */
    static auto create(asio::thread_pool& context, std::size_t data_size)
        -> std::shared_ptr<ThreadPoolEchoServerConnection>;

    /*!
     * \brief Get the socket.
     *
     * \return Socket.
     */
    [[nodiscard]] auto socket() noexcept -> protocol::socket&;

    /*!
     * \brief Start process.
     */
    void start();

private:
    /*!
     * \brief Start to read.
     */
    void do_read();

    /*!
     * \brief Handler called after reading data.
     *
     * \param[in] error Error.
     */
    void on_read(
        const asio::error_code& error, std::size_t /*bytes_transferred*/);

    /*!
     * \brief Handler called after writing data.
     *
     * \param[in] error Error.
     */
    void on_write(
        const asio::error_code& error, std::size_t /*bytes_transferred*/);

    //! Socket.
    protocol::socket socket_;

    //! Strand.
    asio::strand<asio::thread_pool::executor_type> strand_;

    //! Request data.
    std::vector<char> request_;
};

/*!
 * \brief Class of echo server.
 */
class ThreadPoolEchoServer {
public:
    //! Protocol.
    using protocol = asio::ip::tcp;

    /*!
     * \brief Constructor.
     *
     * \param[in] server_endoint Server endpoint.
     * \param[in] data_size Data size.
     * \param[in] num_threads Number of threads.
     */
    ThreadPoolEchoServer(const protocol::endpoint& server_endoint,
        std::size_t data_size, std::size_t num_threads);

    /*!
     * \brief Destructor.
     */
    ~ThreadPoolEchoServer();

    /*!
     * \brief Get the server endpoint.
     *
     * \return Server endpoint.
     */
    [[nodiscard]] auto server_endpoint() -> protocol::endpoint;

    /*!
     * \brief Start the server.
     */
    void start();

    /*!
     * \brief Stop the server.
     */
    void stop();

private:
    /*!
     * \brief Start to accept.
     */
    void do_accept();

    /*!
     * \brief Handler called after accepting a connection.
     */
    void on_accept(const asio::error_code& /*error*/);

    //! Context.
    asio::thread_pool context_;

    //! Strand.
    asio::strand<asio::thread_pool::executor_type> strand_;

    //! Acceptor.
    protocol::acceptor acceptor_;

    //! Next connection.
    std::shared_ptr<ThreadPoolEchoServerConnection> connection_;

    //! Data size.
    std::size_t data_size_;

    //! Flag whether server is running.
    std::atomic<bool> is_running_;
};

}  // namespace server
